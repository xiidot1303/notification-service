# Notification service API

## Setup

Установите зависимости:
```sh
$ pip install -r requirements.txt
```

Создать файл .env со следующими значениями:
```sh
TOKEN=<for Swagger Probe Server>
SECRET_KEY=
```

Migrations:
```sh
$ python manage.py migrate
$ python manage.py makemigrations app
$ python manage.py migrate app 0001
```
Создать пользователя (<user>) Django:
```sh
$ python manage.py createsuperuser
```

Получить ACCESS_TOKEN для отправки запросов
```sh
$ python manage.py drf_create_token <user>
```

## Headers
```
{
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Token <ACCESS_TOKEN>',
}
```

## API Endpoints

`POST /client_create` - Создать клиента  
data:
```
{   
  'phone': '',
  'code': '',
  'tag': '',
  'timezone': ''
}
```
- - -

`DELETE /client_edit/<client_id>` - удалить клиента  
`POST /client_edit/<client_id>` - обновления данных атрибутов клиента  
data:
```
{
  'phone': '',
  'code': '',
  'tag': '',
  'timezone': ''
{
```
- - -

`POST /newsletter_create` - добавить новой рассылку  
data:
```
{
    "start_time": <datetime>,
    "message_text": "",
    "end_time": <datetime>,
    "client_property": {
        "code": "",
        "tag": "",
    }
}
```
- - -

`DELETE /newsletter_edit/<newsletter_id>` - удалить рассылку  
`POST /newsletter_edit/<newsletter_id>` - обновления атрибутов рассылки  
data:
```
{
    "start_time": <datetime>,
    "message_text": "",
    "end_time": <datetime>,
    "client_property": {
        "code": "",
        "tag": "",
    }
}
```
- - -


`GET newsletter_overall_statistics` - статистики по созданным рассылкам  

`GET newsletter_statistic/<newsletter_id>` - детальной статистики отправленных сообщений по конкретной рассылке
- - -



## Details

DATETIME_FORMAT: `%Y-%m-%d %H:%M:%S`
- - - 

## Run
```sh
$ python manage.py runserver
```
